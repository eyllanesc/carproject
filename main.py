from PySide.QtCore import *
from PySide.QtGui import *

from connection import create_connection, QSqlQuery
from lib.views import CategoryView, CarView, CarGraphicsView


class CarWidget(QWidget):
    def __init__(self, *args, **kwargs):
        QWidget.__init__(self, *args, **kwargs)
        lay = QHBoxLayout(self)
        self.categoryView = CategoryView(self)
        self.carView = CarView(self.categoryView.model(), self)
        self.categoryView.model().dataChanged.connect(self.onDataChanged)
        self.loadCarView()

        lay.addWidget(self.categoryView)
        splitter = QSplitter(Qt.Vertical, self)
        carGraphicsView = CarGraphicsView(self.carView, self)
        self.categoryView.model().dataChanged.connect(carGraphicsView.updateItems)
        splitter.addWidget(carGraphicsView)
        splitter.addWidget(self.carView)
        lay.addWidget(splitter)

    def onDataChanged(self, topLeft, bottomRight):
        if topLeft.column() == self.sender().fieldIndex("flag") == bottomRight.column():
            self.loadCarView()

    def loadCarView(self):
        r = QSqlQuery("select category from Category WHERE  flag=1")
        ids = []
        while r.next():
            ids.append(r.value(r.record().indexOf("category")))
        self.carView.model().setFilter("category", ids)


if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    app.setStyle("fusion")
    if not create_connection():
        sys.exit(-1)
    w = CarWidget()
    w.showMaximized()
    sys.exit(app.exec_())
