import json

from PySide.QtGui import QColor, QAbstractProxyModel


def json2db(filename):
    json_data = open(filename).read()
    data = json.loads(json_data)
    categories = {}
    cars = []
    fields = ("category", "weight", "name", "coordinates")
    for element in data:
        category = element["category"]
        color = QColor(*element["color"]).name()
        weight = element["weight"]
        name = element["name"]
        coordinates = element["coordinates"]
        if category in categories:
            _color, number = categories[category]
            assert color == _color
        else:
            categories[category] = color, len(categories)
        _, number = categories[category]
        cars.append(dict(zip(fields, map(str, [number, weight, name, coordinates]))))

    return categories, cars


if __name__ == '__main__':
    json2db("data.json")
