from PySide.QtCore import Qt
from PySide.QtGui import QColor, QSortFilterProxyModel
from PySide.QtSql import QSqlTableModel, QSqlRelationalTableModel, QSqlRelation, QSqlDriver


class CategoryModel(QSqlTableModel):
    def __init__(self, *args, **kwargs):
        QSqlTableModel.__init__(self, *args, **kwargs)
        self.setTable("Category")
        self.setEditStrategy(QSqlTableModel.OnFieldChange)
        self.select()

    def flags(self, index):
        fl = QSqlTableModel.flags(self, index)
        if index.column() == self.fieldIndex("category"):
            fl |= Qt.ItemIsUserCheckable
        return fl

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.CheckStateRole:
            val = self.index(index.row(), self.fieldIndex("flag")).data()
            return Qt.Unchecked if val == 0 else Qt.Checked
        elif role == Qt.DecorationRole:
            color = self.index(index.row(), self.fieldIndex("color")).data()
            return QColor(color)
        return QSqlTableModel.data(self, index, role)

    def setData(self, index, value, role=Qt.EditRole):
        if role == Qt.CheckStateRole:
            ix = self.index(index.row(), self.fieldIndex("flag"))
            self.setData(ix, 0 if value == Qt.Unchecked else 1)
            return True
        elif role == Qt.DecorationRole:
            ix = self.index(index.row(), self.fieldIndex("color"))
            self.setData(ix, value.name())
            return True
        return QSqlTableModel.setData(self, index, value, role)


class CarModel(QSqlRelationalTableModel):
    def __init__(self, model, *args, **kwargs):
        QSqlRelationalTableModel.__init__(self, *args, **kwargs)
        self.setTable("Car")
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.setRelation(self.fieldIndex("category"), QSqlRelation(model.tableName(), "id", "category"))
        self.setEditStrategy(QSqlTableModel.OnManualSubmit)
        self.select()
        self.m = model

    def changed(self, topLeft, bottomRight):
        print(topLeft, bottomRight)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        val = QSqlRelationalTableModel.headerData(self, section, orientation, role)
        if role == Qt.DisplayRole and orientation == Qt.Horizontal:
            val = val.capitalize()
        return val

    def flags(self, index):
        return QSqlRelationalTableModel.flags(self, index) & ~Qt.ItemIsEditable

    def data(self, index, role=Qt.DisplayRole):
        if role == Qt.DecorationRole and index.column() == self.fieldIndex("name"):
            cat = self.data(self.index(index.row(), self.fieldIndex("category")))
            r = self.m.match(self.m.index(0, self.m.fieldIndex("category")), Qt.DisplayRole, cat)[0].row()
            return QColor( self.m.index(r, self.m.fieldIndex("color")).data())
        return QSqlRelationalTableModel.data(self, index, role)


class CarFilterModel(QSortFilterProxyModel):
    def __init__(self, *args, **kwargs):
        QSortFilterProxyModel.__init__(self, *args, **kwargs)
        self.patterns = {}
        self.layoutChanged.connect(lambda : print("sss"))

    def setFilter(self, fieldname, value):
        self.patterns[fieldname] = value
        self.invalidateFilter()

    def filterAcceptsRow(self, sourceRow, sourceParent):
        val = True
        for fieldname, fvalue in self.patterns.items():
            value = self.sourceModel().index(sourceRow, self.sourceModel().fieldIndex(fieldname)).data()
            if value is not None:
                val = val and self.filter(value, fvalue, fieldname)
        return val

    @staticmethod
    def filter(value, fvalue, fielname):
        if fielname == "category":
            return value in fvalue
        return True
