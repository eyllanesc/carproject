import ast

from PySide.QtCore import Qt, QPersistentModelIndex, QModelIndex
from PySide.QtGui import QListView, QMenu, QColorDialog, QTableView, QHeaderView, QGraphicsView, QGraphicsScene, \
    QPixmap, QGraphicsEllipseItem, QPen, QBrush, QGraphicsItem, QAbstractItemView

from .models import CategoryModel, CarModel, CarFilterModel


class CategoryView(QListView):
    def __init__(self, *args, **kwargs):
        QListView.__init__(self, *args, **kwargs)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.onCustomContextMenuRequested)
        model = CategoryModel(self)
        self.setModel(model)
        self.setModelColumn(model.fieldIndex("name"))
        self.setFixedWidth(150)
        self.setModelColumn(model.fieldIndex("category"))

    def onCustomContextMenuRequested(self, point):
        ix = self.indexAt(point)
        menu = QMenu(self)
        color_action = menu.addAction("Change color")
        action = menu.exec_(self.mapToGlobal(point))
        if action == color_action:
            color = ix.data(Qt.DecorationRole)
            n_color = QColorDialog.getColor(color, self)
            self.model().setData(ix, n_color, Qt.DecorationRole)


class CarView(QTableView):
    def __init__(self, m, *args, **kwargs):
        QTableView.__init__(self, *args, **kwargs)
        model = CarModel(m)
        proxyModel = CarFilterModel()
        proxyModel.setSourceModel(model)
        model.layoutChanged.connect(lambda : print("sss"))
        self.setModel(proxyModel)
        self.horizontalHeader().setResizeMode(QHeaderView.Stretch)
        self.hideColumn(0)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setSelectionMode(QAbstractItemView.MultiSelection)


class CarGraphicsScene(QGraphicsScene):
    def __init__(self, *args, **kwargs):
        QGraphicsScene.__init__(self, *args, **kwargs)
        self.mPixmap = QPixmap("/home/eyllanesc/Downloads/pittsburgh.jpg")
        self.setSceneRect(self.mPixmap.rect())

    def drawBackground(self, painter, rect):
        painter.drawPixmap(rect.toRect(), self.mPixmap, rect.toRect())


class CarGraphicsView(QGraphicsView):
    def __init__(self, view, *args, **kwargs):
        QGraphicsView.__init__(self, *args, **kwargs)
        self.setDragMode(QGraphicsView.RubberBandDrag)
        self.mod = view.model()
        self.view = view
        self.setScene(CarGraphicsScene())
        self.setCacheMode(QGraphicsView.CacheBackground)
        self.updateItems()
        self.scene().selectionChanged.connect(self.onSelectionChanged)

    def updateItems(self):
        model = self.mod
        coordColumn = model.sourceModel().fieldIndex("coordinates")
        nameColumn = model.sourceModel().fieldIndex("name")
        self.scene().clear()

        for i in range(model.rowCount()):
            ix = model.index(i, coordColumn)
            color = model.index(i, nameColumn).data(Qt.DecorationRole)
            x, y = ast.literal_eval(ix.data())
            item = self.scene().addEllipse(x, y, 20, 20, QPen(color), QBrush(color))
            item.setFlag(QGraphicsItem.ItemIsSelectable)
            item.setData(Qt.UserRole+1, QPersistentModelIndex(ix))

    def onSelectionChanged(self):
        self.view.clearSelection()
        for item in self.scene().selectedItems():
            p_ix = item.data(Qt.UserRole+1)
            ix = QModelIndex(p_ix)
            self.view.selectRow(ix.row())

    def wheelEvent(self, event):
        # zoom factor
        factor = 1.25

        # Set Anchors
        self.setTransformationAnchor(QGraphicsView.NoAnchor)
        self.setResizeAnchor(QGraphicsView.NoAnchor)

        # Save the scene pos
        oldPos = self.mapToScene(event.pos())

        # Zoom
        if event.delta() < 0:
            factor = 1.0 / factor
        self.scale(factor, factor)

        # Get the new position
        newPos = self.mapToScene(event.pos())

        # Move scene to old position
        delta = newPos - oldPos
        self.translate(delta.x(), delta.y())

    def mouseDoubleClickEvent(self, event):
        self.fitInView(self.sceneRect(), Qt.KeepAspectRatio)
        QGraphicsView.mouseDoubleClickEvent(self, event)