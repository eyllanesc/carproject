from PySide.QtCore import *
from PySide.QtGui import *
from PySide.QtSql import *

from utils import json2db


def create_connection():
    db = QSqlDatabase.addDatabase("QSQLITE")
    db.setDatabaseName("data.db")
    if not db.open():
        QMessageBox.critical(None, "Cannot open database",
                             """Unable to establish a database connection.\n
                             This example needs SQLite support. Please read 
                             the Qt SQL driver documentation for information how
                             to build it.\n\n Click Cancel to exit.""",
                             QMessageBox.Cancel)

        return False

    categories, cars = json2db("data.json")

    query = QSqlQuery()
    query.exec_("create table IF NOT EXISTS Car (id INTEGER PRIMARY KEY AUTOINCREMENT, "
                "name varchar(20), weight varchar(20), category INTEGER, coordinates VARCHAR(18))")

    query.exec_("create TABLE IF NOT EXISTS Category (id int PRIMARY  KEY,"
                "category VARCHAR(20), color VARCHAR(7), flag INTEGER DEFAULT 0)")

    return True
    for category, val in categories.items():
        color, id = val
        query.exec_("insert into Category(id, category, color) values(%s, '%s', '%s')" % (id, category, color))

    for car in cars:
        values = tuple(car.values())
        query.exec_("insert into Car(category, weight, name, coordinates) values(%s, '%s', '%s', '%s')" % values)
    return True
